import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment'; 
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:HttpClient) { }

  post(data:any)
  {
  
  this.http.post<any>(environment.baseUrl+"users",data).subscribe(
    res=>{
      console.log(res)
    },
    err=>{
      console.log(err)
    }
  )

  }
 login(data:any){
  return this.http.post<any>(environment.baseUrl+"auth/login",data)
  }
}
