import { Injectable } from '@angular/core';
declare var require:any
const $=require("jquery")
@Injectable({
  providedIn: 'root'
})
export class JqueryService {

  constructor() { 
    
  }

  jq=()=>
  {
  setTimeout(()=>{
    $('#mult')?.multiselect({
      selectColor: 'purple',
      selectSize: 'small',
      selectText: 'Select Project',
      duration: 300,
      easing: 'slide',
      listMaxHeight: 300,
      selectedCount: 2,
      sortByText: true,
      fillButton: true,
      data: {
        "BD": "Bangladesh",
        "BE": "Belgium",
        "BF": "Burkina Faso",
        "BG": "Bulgaria",
        "BA": "Bosnia and Herzegovina",
        "BB": "Barbados",
        "WF": "Wallis and Futuna",
        "BL": "Saint Barthelemy",
        "BM": "Bermuda",
      },
      onSelect: function(values:any) {
        console.log('return values: ', values);
      }
      });
      $('#get_values').on('click', function(event:any) {
                console.log($('#multi').multi_select('getSelectedValues'));
        $('.data-display').remove();
        var json = { items: $('#multi').multi_select('getSelectedValues') };
        if (json.items.length) {
          var ul = $('<ul>', { 'class': 'data-display' }).appendTo('body');
          $(json.items).each(function(index:any, item:any) {
            ul.append(
              '<li style="display: block;">' + item + '</li>'
            );
          });
        }
      })
    $('#clear_values').on('click', function(event:any) {
      $('#multi').multi_select('clearValues');
      $('.data-display').slideUp(300, function(ev:any) {
        $(ev).remove()
      })
    })
  },4000)
  }
}
