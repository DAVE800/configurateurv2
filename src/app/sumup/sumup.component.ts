import { Component, OnInit,Input,ViewChild,ElementRef } from '@angular/core';
import jsPDF from 'jspdf';
declare var require:any;
var pdfMake=require("pdfmake/build/pdfmake")
var pdfFonts =require('pdfmake/build/vfs_fonts');
pdfMake.vfs = pdfFonts.pdfMake.vfs;
;
var htmlToPdfmake = require('html-to-pdfmake');
@Component({
  selector: 'app-sumup',
  templateUrl: './sumup.component.html',
  styleUrls: ['./sumup.component.css']
})

export class SumupComponent implements OnInit {
@Input() data:any
 date :any
  constructor(private pdfTable:ElementRef) { }
  ngOnInit(): void {
    console.log(this.data)
    this.date=new Date()

  }


  public downloadAsPDF() {
  //  const doc = new jsPDF();
    const pdfTable:any = document.getElementById("pdfTable");
    var html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open(); 
  }
}
