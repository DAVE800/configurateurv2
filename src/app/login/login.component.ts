import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { HttpService } from '../core/http.service';

declare var require:any
var $ = require('jquery')
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
@Output() authEvent =new EventEmitter<boolean>()
@Output() registerEvent =new EventEmitter<boolean>()
 data :any={
   email:"",
   password:""
 }
  isauth=true
  regist=true
  constructor(private http:HttpService ) { }

  ngOnInit(): void {
  
  }

  async login(event:any){
    await this.http.login(this.data).subscribe(
      res=>{
        res=JSON.parse(res)
        if(res.status){
          console.log(res)
          this.authEvent.emit(event)
        }else{
          console.log(res)
        }
      },
      err=>
      {
        console.log(err.error);
        
      }
    )
     //
  }

  recoverpwd(){
    this.isauth=false
  }
  
register(val:any){
  this.registerEvent.emit(val)
}
}
