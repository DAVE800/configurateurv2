import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { HttpService } from '../core/http.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
@Output() registerEvent=new EventEmitter<any>();
@Output() loginEvent=new EventEmitter<boolean>();
letmelogin=false;
 registered=false;
 data={
  username:"",
  email:"",
  passord:"",
  first_name:"",
  last_name:"",
  age:"",
  postcode:"",
  address:"",
  company:"",
  city:"",
  phone:"",
 }

  constructor(private http:HttpService) { }
 
  ngOnInit(): void {
  }

  register(value:any){
    var  err=false
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(typeof(this.data.age)!="number"){
    err=true
    }
   
  
      this.data.email=this.data.email.toLowerCase()
    if(!emailPattern.test(this.data.email)){
      console.log("oooo!!!!!!!!!!!!!!!!!!!!!")
      err=true
    }
   
    if(!err){
      this.http.post(this.data)
      Object.assign(this.data,{value:value})
      this.registerEvent.emit(this.data)
    }

  }

  login(value:any){
    this.loginEvent.emit(value)
  }

}
