import { Component, OnInit,Output,EventEmitter, Input,OnChanges,SimpleChanges, } from '@angular/core';
import { fabric } from 'fabric';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {JqueryService}  from "src/app/core/jquery.service"
declare var require:any
var $ = require('jquery')
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
 @Input() user:any
  canvas:any
  custom="custom"
  width=400;
  height=400;
  isauth=true
  sum=false
  data:any={}
  dropdownList :any= [];
  selectedItems :any= [];
  selectedItems3 :any= [];
  selectedItems1 :any= [];
  selectedItems2 :any= [];
  selectedItems4 :any= [];
  selectedItems5 :any= [];
  selectedItems6 :any= [];
  selectedItems7 :any= [];

  dropdownList1:any=[]
  dropdownList2:any=[]
  dropdownList3:any=[]
  dropdownList4:any=[]
  dropdownList5:any=[]
  dropdownList6:any=[]
  dropdownList7:any=[]

  dropdownSettings :IDropdownSettings= {};
  @Output() sumUpevent=new EventEmitter<boolean>()
  title = 'configurateur';
 
   infop={
    last_name:"",
    first_name:"",
    company:"",
    city:"",
    address:"",
    postcode:"",
    age:"",
    email:"",
    phone:"",

  }
  infoR={
   height:0.5,
   weight:1.7,
  size:0.5,
  sizew:4.6,
  age:89
  }

  checkbox={
    lombalgie:false,
    nevralgiesPudendalesPerineales:false,
    coccygodynieEndometriose:false,
    arthrodeseGenouxHanche:false,
    cervicale:false,
    arthodesedlombaire:false,
    FractureDuBassin:false,
    Discopathie:false,
    ProtheseJambes:false,
    PersonneLarge:false,
    RegionSacrum:false,
    SciatiqueEtCruralgie:false,
    ProtheseDeGenoux:false,
    RegionSacroIliacques:false,
    RegionCoxoFemorale:false,
    RegionScolioseEtCyphose:false,
    RegionQueueDeCheval:false,
    PersonneFine:false
    
  }
  info=(sum:boolean)=>{
    this.sum=true
    this.sumUpevent.emit(sum)
    console.log(this.infoR)
    console.log(this.infop)
    console.log(this.checkbox)
    var tab:any=[] 
    tab.push(this.selectedItems,this.selectedItems1,this.selectedItems2,this.selectedItems3,this.selectedItems4,this.selectedItems5,this.selectedItems6,this.selectedItems7)
    Object.assign(this.data,this.infoR,this.infop,this.checkbox,{url:this.canvas.toDataURL(),tab:tab})
   console.log(this.data.tab)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.infop=changes.user.currentValue
  }
  ngOnInit(): void {
   // this.data=this.user
    this.dropdownList = [
      { item_id: 1, item_text: 'Coup de poignard' },
      { item_id: 2, item_text: 'Brulure' },
      { item_id: 3, item_text: 'Picotement ou une gène' },
    ];
    this.dropdownList1 = [
      { item_id: 1, item_text: 'Extra ferme' },
      { item_id: 2, item_text: 'Ferme' },
      { item_id: 3, item_text: 'Souple' },
    ];
    this.dropdownList2 = [
      { item_id: 1, item_text: 'S8 POUR RÉÉQUILIBRER LE BASSIN' },
      { item_id: 2, item_text: 'PAR 2 POCHE GONFLABLE' },
    ];

    this.dropdownList3 = [
      { item_id: 1, item_text: 'S70 SOULAGER  OMOPLATE' },
      { item_id: 2, item_text: 'PAR 2 POCHE GONFLABLE' },
    ];
    this.dropdownList4 = [
      { item_id: 1, item_text: 'S8 POUR RÉÉQUILIBRER LE BASSIN' },
      { item_id: 2, item_text: 'PAR 2 POCHE GONFLABLE' },
    ];
    this.dropdownList5 = [
      { item_id: 1, item_text: 'VIRGULE' },
      { item_id: 2, item_text: 'PAPILLON' },
      { item_id: 2, item_text: 'ARC FLEXIO VISCO' },
    ];
    this.dropdownList7 = [
      { item_id: 1, item_text: 'Dossier' },
      { item_id: 2, item_text: 'Tissu non feu' },
      { item_id: 2, item_text: 'Autre' },

    ];


    this.dropdownList6 = [
      { item_id: 1, item_text: 'S7 SOULAGER  LOMBAIRES' },
      { item_id: 2, item_text: 'PAR 2 POCHE GONFLABLE' },
    ];
   this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    setTimeout(() => {
        this.canvas= new fabric.Canvas('pain',{
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor:'blue',
        fireRightClick: true,
        preserveObjectStacking: true,
        stateful:true,
        stopContextMenu:false,   
      });
      this.canvas.setWidth(this.width);
      this.canvas.setHeight(this.height);
      this.draw()
      this.eventbiding()
    },
    1000);
   
   
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onItemSelect1(item: any) {
    console.log(item);
  }

  onItemSelect2(item: any) {
    console.log(item);
  }

  onItemSelect3(item: any) {
    console.log(item);
  }

  onItemSelect4(item: any) {
    console.log(item);
    this.selectedItems4.push(item)

  }
  onItemSelect5(item: any) {
    console.log(item);
    this.selectedItems5.push(item)


  }

  onItemSelect6(item: any) {
    console.log(item);
    this.selectedItems6.push(item)

  }

  onItemSelect7(item: any) {
    console.log(item);
    this.selectedItems7.push(item)

  }
  
  

  
 
  onSelectAll(items: any) {
    console.log(items);
    if(this.selectedItems.length==0)
    {
      this.selectedItems.push(items)

    }else{
      this.selectedItems=[]
      this.selectedItems.push(items)

    }
  }
 
  onSelectAll1(items: any) {
    console.log(items);
    if(this.selectedItems1.length==0)
    {
      this.selectedItems1.push(items)

    }else{
      this.selectedItems1=[]
      this.selectedItems1.push(items)

    }
  }


onSelectAll2(items: any) {
    console.log(items);
    if(this.selectedItems2.length==0)
    {
      this.selectedItems2.push(items)

    }else{
      this.selectedItems2=[]
      this.selectedItems2.push(items)

    }
  }
 
  
  onSelectAll3(items: any) {
    console.log(items);
    if(this.selectedItems3.length==0)
    {
      this.selectedItems3.push(items)

    }else{
      this.selectedItems3=[]
      this.selectedItems3.push(items)

    }
  }  

  onSelectAll4(items: any) {
    console.log(items);
    if(this.selectedItems4.length==0)
    {
      this.selectedItems4.push(items)

    }else{
      this.selectedItems4=[]
      this.selectedItems4.push(items)

    }
  }
 
  onSelectAll5(items: any) {
    console.log(items);
    if(this.selectedItems5.length==0)
    {
      this.selectedItems5.push(items)

    }else{
      this.selectedItems5=[]
      this.selectedItems5.push(items)

    }
  }

  onSelectAll6(items: any) {
    console.log(items);
    if(this.selectedItems6.length==0)
    {
      this.selectedItems6.push(items)

    }else{
      this.selectedItems6=[]
      this.selectedItems6.push(items)

    }
  }
  

  onSelectAll7(items: any) {
    console.log(items);
    if(this.selectedItems7.length==0)
    {
      this.selectedItems7.push(items)

    }else{
      this.selectedItems7=[]
      this.selectedItems7.push(items)

    }
  }
  login(auth:any){
    this.isauth=auth
  }


  eventbiding(){
    var blackpain= document.getElementById("blackpain");
    var redpain= document.getElementById("redpain");

    blackpain?.addEventListener("click",(e)=>{
      e.preventDefault()
      var url = "assets/img/cross.png"
      console.log(e)
      fabric.Image.fromURL(url,(img) => {
          
        this.canvas.add(img).setActiveObject(img);
        this.canvas.centerObject(img);
        this.canvas.renderAll(img);
        },
        {
           scaleX:0.2,     
           scaleY:0.2,
           crossOrigin: "Anonymous",
        
           
        }
        );


       })

       redpain?.addEventListener("click",(e)=>{
        e.preventDefault()

        var url = "assets/img/cross.jpeg"
        console.log(e)
        fabric.Image.fromURL(url,(img) => {
            
           this.canvas.add(img).setActiveObject(img);
          this.canvas.centerObject(img);
          this.canvas.renderAll(img);
          },
          {
             scaleX:0.2,     
             scaleY:0.2,
             crossOrigin: "Anonymous",
          
             
          }
          );


         })
  }


  draw=()=>{

    var img = new Image();
    img.src="/assets/img/plan.png"
    img.crossOrigin ='anonymous';
   img.onload = async  ()=> {
    var Img = new fabric.Image(img);
   await this.canvas.setBackgroundImage(Img,this.canvas.renderAll.bind(this.canvas), {
            backgroundImageOpacity: 0.1,
            backgroundImageStretch: true,
            left:0,
            top:5,
            right:5,
            bottom:0,    
            scaleX: this.canvas.width / img.width,
            scaleY: this.canvas.height /img.height
            
        });
        this.canvas.centerObject(Img);
        this.canvas.renderAll(Img);
        this.canvas.requestRenderAll(); 
      }

  }

  
}
